var controller = require('/controller/hello');
var htmlController = require('/controller/html');
var messagesController = require('/controller/messages');
var core = require('/lib/core');
var http = require('/lib/http');
var router = require('/lib/router')();
var websocket = require('/lib/websocket');

var text = 'empty';


/*router.filter = function(req, next){
	var start = Date.now();
	var res = next(req);
	res.body.start = start;
	return res;
};*/

router.filter = function(req, next){
	return next(req);
};

router.get('/welcome', function(){
	return {
		body: {
			message: 'Welcome'
		}
	} 
});

router.get('/dragon', function(req){
	var post = htmlController.html('post.html');
	var dragon = core.loadResource(resolve('/banner.txt'));
	return {
		body: post + '<pre>'+core.readText(dragon)+'</pre>',
		contentType: 'text/html'
	}
});


router.get('/fish', function(req){
	var post = htmlController.html('post.html');
	var fish = core.loadResource(resolve('/assets/fish.txt'));
	return {
		body: post + '<pre>'+core.readText(fish)+'</pre>',
		contentType: 'text/html'
	}
});

router.get('/', function(req){
	return {
		redirect: '/post'
	}
});

router.get('/post', function(req){
	var post = htmlController.html('post.html');
	var messages = messagesController.get();

	return {
		body: post + messages,
		contentType: 'text/html'
	}
});

router.get('/persons', function(){
	return {
		body: {
			name: 'Sheldon',
			age: 42
		}
	} 
});

router.get('/postit', function(req){
	messagesController.add(req.params.msg);

	if (req.params.msg == 'dragon'){
		return {redirect: '/dragon'}
	}

	if (req.params.msg == 'fish'){
		return {redirect: '/fish'}
	}

	return {
		redirect: '/post'
	}
});

exports.post = function(req){
	log.info(JSON.stringify(req, 4, null));
	messagesController.add('test');
	return {
		redirect: '/post'
	}
};

/*exports.get = function(req){
	
	if (req.webSocket){
			return {
				webSocket: {
					timeout: 30000,
					group: 'fisk',
					attributes: {
						a:1,
						b:2
					}
				}
			}
	}else{
		websocket.sendToGroup('fisk', 'Someone accessed ' + req.uri);
		return controller.hello(req);	
	}
};
*/

exports.webSocketEvent = function(event){
	log.info(JSON.stringify(event));
	if (event.type == 'open'){
		websocket.send(event.session.group, 'Welcome');
	}else if (event.type == 'message'){
		websocket.sendToGroup(event.session.group, event.message, event.session.id);
	}
	
};

/*exports.post = function(req){
	log.info(req);
	//text = http.getMultipartItem('upload',0).stream;
	//log.info(text);
};*/

router.get('/persons/{id:[0-9]{2}}', function(req){
	return {
		body: {
			id: req.pathParams.id
		}
	} 
});


exports.service = function(req){
	return router.dispatch(req);
};

