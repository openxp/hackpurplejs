var core = require('/lib/core');

exports.html = function(htmlfile){
    var resource = core.loadResource(resolve('/assets/'+htmlfile));
    return core.readText(resource);
};