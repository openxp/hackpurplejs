package foo.bar;

import io.purplejs.core.EngineBinder;
import io.purplejs.core.EngineModule;

public class MyModule implements EngineModule{

    @Override
    public void configure(EngineBinder engineBinder) {
        engineBinder.initializer(engine -> {
            engine.getSettings();
        });
    }
}